/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Inventario;

import java.util.Scanner;
import java.util.ArrayList;
import java.awt.*;
import java.awt.event.*;
import java.util.Locale;
import javax.swing.*;


/**
 *
 * @author injotaese
 */


public class Inventario extends Frame implements WindowListener, ActionListener{
    
    public Inventario(){
        super("Inventario");
        this.addWindowListener(this);
        this.setLayout(new FlowLayout());
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setSize(dim.width/3, dim.height/5);
        
        JLabel titulo = new JLabel("Bienvenido");
        this.add(titulo);
        JTextField usr = new JTextField(35);
        this.add(usr);
        JPasswordField pwd = new JPasswordField(35);
        this.add(pwd);
        JButton aceptar = new JButton("Ingresar");
        aceptar.addActionListener(this);
        JButton registro = new JButton("Registrarse");
        registro.addActionListener(this);
        this.add(aceptar);
        this.add(registro);
        this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
        this.setVisible(true);
    }
    
    
    @Override public void actionPerformed(ActionEvent e){
        String cmd = e.getActionCommand().toUpperCase();
        if(cmd.equals("INGRESAR")){
            System.out.println("Click en ingresar");
        } else if(cmd.equals("REGISTRARSE")){
            System.out.println("Click en registrar");
        }
    }
    
    @Override
    public void windowOpened(WindowEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void windowClosing(WindowEvent e) {
        System.exit(0);
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void windowClosed(WindowEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void windowIconified(WindowEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void windowActivated(WindowEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    
    public static void main(String[] args){
        Inventario mainapp = new Inventario();
    }

    
    
}
