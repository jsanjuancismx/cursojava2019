
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Inventario;

import java.util.Scanner;
import java.util.ArrayList;
import java.awt.*;
import java.awt.event.*;
import java.util.Locale;
import javax.swing.*;


/**
 *
 * @author injotaese
 */


/*  TEMAS SIGUIENTE CLASE
    1- Temas avanzados de clases  (herencia, instancias e interfaces)
    2- recursividad
    3- Interfáz grafica (FORMS)
*/
@hdbhd
public class Inventario extends Frame implements WindowListener,ActionListener{
    
    int prodid = 1;
   
    
    
    
    public Inventario(){
        super("My App");
        //addWindowListener(this);
        setLayout(new FlowLayout());
       
        
        setSize(400,400);
        setVisible(true);
        
        JTextField usr = new JTextField(30);
        add(usr);
        
        
        JTextField pwd = new JTextField(30);
        add(pwd);
        
        Button login = new Button("Ingresar");
        add(login);
        login.addActionListener(this);
        
        Button cancelar = new Button("Cancelar");
        add(cancelar);
        cancelar.addActionListener(this);
        
        this.add(login);
                
        
        Dimension dim = getSize();
        dim.width = dim.width*2;
        setSize(dim);
        /*TextField usuario = new JTextField(30);
        usuario.addActionListener(this);
        add(usuario);
        JTextField password = new JTextField(30);
        password.addActionListener(this);
        add(password);
        Button botonAceptar = new Button("Aceptar");
        add(botonAceptar);
        botonAceptar.addActionListener(this);*/
    }
    
    

    private void encenderMotor(){
        
    }
    
  
    public void licuar(){
        this.encenderMotor();
    }
    
    public static void fibonacci(int longitud){
        int a=0,b=1, c=1;
        for(int f = 0; f< longitud; f++){
            System.out.println(c+",");
            c = a+b;
            a = b;
            b = c;
            
        }
    }
    
    
    
    public static int fibonacciR(int longitud, int num){
        
        if(longitud > 0){
            int newnum = num + fibonacciR( longitud - 1, num);
            System.out.println(newnum);
            return newnum;
        } else return 0;   
    }
    
    public static void imprimirArreglo(int[] numeros, int indice){
        if(indice < numeros.length){
            System.out.println("N= "+ numeros[indice]);
            imprimirArreglo(numeros, indice + 2 );
        }
    }
    
    
    public String generarCodigo(){
        return "P"+ this.prodid++;
    }

    public static int menu(Scanner teclado){
        System.out.println("Menú:\n0: Crear\n1:Editar precio\n2:Listar\n5:salir\n ¿opción? -> ");
        int opciontecleada = teclado.nextInt();
        return opciontecleada;
    }
    
    public static int multiplicar(int num){
        System.out.println(num*2);
        return num * 2;
    }
    
    public static int sumar(int num){
        System.out.println(num+2);
        return num + 2;
    }
    
    public static int asignar(int index, int min, int max){
        
        if(index < max){
            return index + asignar(index+1, min, max);
        }
        return index;
    }
    
    public static int asignar2(int index, int[] nums){
        if(index < nums.length){
            nums[index]=  asignar2(index+1, nums);
        }
        return index;
    }
    
    public static int buscarnum(int buscado, int[] nums){
        for(int f =0; f<nums.length; f++){
            System.out.println("buscando en la posicion "+ f);
            if( nums[f] == buscado){
                System.out.println("lo encontré en la posicion "+ f);
                return f;
            }
        }
        System.out.println("No encontré el numero " + buscado);
        return -1;

    }
    
    public static void main(String[] args) {
        
        Inventario app = new Inventario();
       
   
        
        //fibonacciR(10,1);
        
        /*int[] nums = new int[10];
        for(int f = 0; f< nums.length; f++){
            System.out.println("Hola Julio");
            System.out.println("Hola Eric");
            
        }*/
        //imprimirArreglo(nums, 0);
        String nombre = "julio@injotaese.com";
        
        
        System.out.println("_______________");
        /*Inventario inventario = new Inventario();
        Scanner teclado = new Scanner(System.in); // objeto para lectura desde teclado 
        int opcion = 5;
        ArrayList<Producto> losproductos = new ArrayList<Producto>();
        int[] numeros = new int[10];
        System.out.println("arraylength. "+ numeros.length);
        
        int resultado = asignar2(0, numeros);
        System.out.println("resultado:"+ resultado);
        int buscado = 17;
        
        int pos = buscarnum(buscado, numeros);
        if(pos == -1){
            
        } */
        
        
        /** ACTIVIDADES SUGERIDAS
         * 1-   CAMBIAR LOS CASE DE ABAJO PARA QUE FUNCIONEN CON ARRAYLIST 'losproductos' EN LUGAR DEL ARRAY 'productos'
         * 2-   AGREGAR AL MENÚ LA OPCIÓN ELIMINAR, QUE PREGUNTE LA CLAVE DEL PRODUCTO QUE SE DESEA ELIMINAR
         * 3-   HAY QUE AGREGAR LA PROPIEDAD A LA CLASE PRODUCTO QUE SEA UN ENTERO LLAMADO 'existencia'
         * 4-   AGREGAR LA OPCIÓN QUE PERMITA EDITAR LA EXISTENCIA DEL PRODUCTO, QUE PREGUNTE LA CLAVE DEL PRODUCTO QUE SE QUIERE MODIFICAR SU EXISTENCIA
         */
        /*do{
            opcion = menu(teclado);
            switch(opcion){
                case 0:
                    System.out.println("Teclee el marca del producto: ->    ");
                    String marca  = teclado.next();
                    System.out.println("Teclee el nombre del producto: ->    ");
                    String nombre  = teclado.next();
                    System.out.println("Teclee el precio del producto: ->    ");
                    float precio  = teclado.nextFloat();
                    Producto nuevoprod = new Producto(inventario.generarCodigo(), marca, nombre, precio);
                    losproductos.add(nuevoprod);
                break;
                case 1: //editar precio macbook
                    for(int p = 0; p<9; p++){
                        productos[p].imprimir();
                    }
                    System.out.println("Teclee la clave del producto a editar: ");
                    String clave = teclado.next();
                    Producto elproducto = null;
                    int laposicion = 0;
                    for(int p = 0; p<9; p++){
                        if(clave.equals(productos[p].codigo)){
                            laposicion = p;
                        }
                    }
                   
                    System.out.println("\nTeclee el nuevo nombre");
                    String newnombre = teclado.next();
                    productos[laposicion].setName(newnombre);
                    System.out.println("\nTeclee el nuevo precio de "+ productos[laposicion].nombre);
                    float newprecio = teclado.nextFloat();
                    productos[laposicion].setPrecio(newprecio);
                break;
            
                case 2: //listar productos 
                    System.out.println("**************************************");
                    System.out.println("Clave\teMarca\tNombre\t\tPrecio");
                    System.out.println("_____________________________________");
                    for(int p = 0; p<9; p++){
                        productos[p].imprimir();
                    }
                    System.out.println("**************************************");
                break;
            
            }
        } while(opcion != 5); // si se teclea 5 el programa termina. */
        
    }

    @Override
    public void windowOpened(WindowEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void windowClosing(WindowEvent e) {
        dispose();
        System.exit(0);
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void windowClosed(WindowEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void windowIconified(WindowEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void windowActivated(WindowEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println(e.getActionCommand());
        String cmd = e.getActionCommand().toUpperCase();
        
        if(cmd.equals("ACEPTAR")){
            
        } else if(cmd.equals("CANCELAR")){
            Dimension d = this.getSize();
            d.width*=2;
            this.setSize(d);
        }
       
        
        System.out.println("Click");
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

   

   
}
