/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Inventario;

/**
 *
 * @author injotaese
 */
public class Producto {
    String codigo = null;
    String marca = null;
    String nombre = null;
    float precio = (float)0.0;
   
    
    public void reset(){
        this.marca = "";
        this.nombre = "";
        this.precio = (float)0.0;
    }
    
    
    public Producto(String cod, String mar, String nom, float prec){
        this.codigo = cod;
        this.marca = mar;
        this.nombre = nom;
        this.precio = prec;
    }
    
    public void setName(String name){
        this.nombre = name;
    }
    public void setPrecio(float precio){
        this.precio = precio;
    }
    
    public void generarCodigo(int posicion){
        this.codigo = "P"+posicion;
    }
    
    public void imprimir(){
        System.out.println(this.codigo+"\t"+this.marca + "\t" + this.nombre + "\t$ " + this.precio); 
    }
    
    public static void main(){
        System.out.println("Hola mundo");
    }
}
